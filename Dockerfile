FROM openjdk:17-oracle
EXPOSE 8080
RUN mkdir -p /app/
ADD build/libs/bancobase-test-0.0.1-SNAPSHOT.jar /app/bancobase-test-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java", "-jar", "/app/bancobase-test-0.0.1-SNAPSHOT.jar"]