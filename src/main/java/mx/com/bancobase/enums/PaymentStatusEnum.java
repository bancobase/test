package mx.com.bancobase.enums;

import java.io.Serializable;

public enum PaymentStatusEnum implements Serializable {
    TRANSFERRED, CANCELLED, REFUNDED, INSUFFICIENT_FUNDS
    ;
}
