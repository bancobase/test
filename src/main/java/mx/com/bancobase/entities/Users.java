package mx.com.bancobase.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.sql.Date;

@Entity
@Table(name="users")
@Getter
@Setter
public class Users implements Serializable {
    @Id
    @Column(name="user_id", unique=true, nullable=false)
    private Integer userId;

    @Column(name="name", length=20, nullable = false)
    private String name;
    @Column(name="lastname", length=20, nullable = false)
    private String lastname;
    @Column(name="sex", nullable = false)
    private Short sex;
    @Column(name="birth", nullable = false)
    private Date birth;
}
