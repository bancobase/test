package mx.com.bancobase.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

@Entity
@Table(name="accounts")
@Getter
@Setter
public class Accounts implements Serializable {
    @Id
    @Column(name="account_id", unique=true, nullable=false)
    private Integer accountId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private Users user;
    @Column(name="amount", nullable = false)
    private BigDecimal amount;
    @Column(name="creation_account", nullable = false)
    private Date creationAccount;
}
