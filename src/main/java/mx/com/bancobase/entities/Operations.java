package mx.com.bancobase.entities;

import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

@Entity
@Table(name="operations")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Operations implements Serializable {
    @Id
    @Column(name="operation_id", unique=true, nullable=false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer accountId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "account_id_transmitter")
    private Accounts transmitter;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "account_id_receptor")
    private Accounts receptor;
    @Column(name="amount", nullable = false)
    private BigDecimal amount;
    @Column(name="concept", nullable = false)
    private String concept;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "status")
    private StatusOperationsCat status;
    @Column(name="operation_date", nullable = false)
    private Date operationDate;
}
