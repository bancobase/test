package mx.com.bancobase.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Entity
@Table(name="status_operation_cat")
@Getter
@Setter
public class StatusOperationsCat implements Serializable {
    @Id
    @Column(name="status_op_id", unique=true, nullable=false)
    private Short statusOpId;

    @Column(name="name", length=20, nullable = false)
    private String name;
    @Column(name="key_name", length=20, nullable = false)
    private String keyName;
}
