package mx.com.bancobase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BancoBaseApplication {
	public static void main(String[] args) {
		SpringApplication.run(BancoBaseApplication.class, args);

	}
}
