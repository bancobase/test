package mx.com.bancobase.repository;

import mx.com.bancobase.entities.Operations;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface OperationsRepository extends JpaRepository<Operations, Integer> {
}
