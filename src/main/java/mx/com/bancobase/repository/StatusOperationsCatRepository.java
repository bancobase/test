package mx.com.bancobase.repository;

import mx.com.bancobase.entities.StatusOperationsCat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
@Transactional
public interface StatusOperationsCatRepository extends JpaRepository<StatusOperationsCat, Short> {

    Optional<StatusOperationsCat> findByKeyName(String keyName);
}
