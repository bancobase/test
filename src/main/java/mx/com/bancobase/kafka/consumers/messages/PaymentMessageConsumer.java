package mx.com.bancobase.kafka.consumers.messages;

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class PaymentMessageConsumer {

    @KafkaListener(topics = "${spring.kafka.template.default-topic}", groupId = "${spring.kafka.consumer.group-id}")
    public void listen(String message) {
        log.info("KAFKA -> Update payment bancobase: {}", message);
    }

}
