package mx.com.bancobase.utils;

import mx.com.bancobase.exceptions.ParamTypeException;

import java.sql.Date;

public class BancoBaseUtils {

    public static Integer getIntegerVaue(String value) throws ParamTypeException {
        try {
            return Integer.valueOf(value);
        } catch (NumberFormatException nfe) {
            throw new ParamTypeException("El parámetro no es de tipo Entero");
        }
    }

    public static Date getCurrentDate() {
        return new java.sql.Date(new java.util.Date().getTime());
    }
}
