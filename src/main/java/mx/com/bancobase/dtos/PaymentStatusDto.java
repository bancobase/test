package mx.com.bancobase.dtos;

import com.google.gson.Gson;
import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PaymentStatusDto implements Serializable{
	@Getter
	@Setter
	private Integer originAccount;
	@Getter
	@Setter
	private String originNameAccount;
	@Getter
	@Setter
	private Integer targetAccount;
	@Getter
	@Setter
	private String targetNameAccount;
	@Getter
	@Setter
	private BigDecimal amount;
	@Getter
	@Setter
	private String concept;
	@Getter
	@Setter
	private String status;
	@Getter
	@Setter
	private Date operationDate;

	@Override
	public String toString() {
		return new Gson().toJson(this);
	}
}