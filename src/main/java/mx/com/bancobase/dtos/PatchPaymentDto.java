package mx.com.bancobase.dtos;

import com.google.gson.Gson;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.*;
import mx.com.bancobase.enums.PaymentStatusEnum;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PatchPaymentDto implements Serializable{
	@Getter
	@Setter
	@Positive(message = "El parámetro accountId debe ser entero, positivo y mayor a cero")
	@NotNull(message = "El parámetro accountId no puede ir vacío")
	private Integer accountId;
	@Getter
	@Setter
	@NotNull(message = "El parámetro status no puede ir vacío")
	private PaymentStatusEnum status;

	@Override
	public String toString() {
		return new Gson().toJson(this);
	}
}