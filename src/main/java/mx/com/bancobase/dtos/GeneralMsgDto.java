package mx.com.bancobase.dtos;

import lombok.*;
import org.springframework.http.HttpStatus;

import java.io.Serial;
import java.io.Serializable;

@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GeneralMsgDto implements Serializable {

	/**
	 *
	 */
	@Serial
	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	private String msg;

	@Getter
	@Setter
	private HttpStatus httpStatus;
}
