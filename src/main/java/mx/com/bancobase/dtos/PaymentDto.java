package mx.com.bancobase.dtos;

import com.google.gson.Gson;
import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.*;
import mx.com.bancobase.enums.PaymentStatusEnum;

import java.io.Serializable;
import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PaymentDto implements Serializable{
	@Getter
	@Setter
	@NotBlank(message = "El parámetro concept no puede ir vacio")
	private String concept;
	@Getter
	@Setter
	@Positive(message = "El parámetro originAccount debe ser entero, positivo y mayor a cero")
	@NotNull(message = "El parámetro originAccount no puede ir vacío")
	private Integer originAccount;
	@Getter
	@Setter
	@Positive(message = "El parámetro targetAccount debe ser entero, positivo y mayor a cero")
	@NotNull(message = "El parámetro targetAccount no puede ir vacío")
	private Integer targetAccount;
	@Getter
	@Setter
	@DecimalMin(value = "0.0", inclusive = false, message = "El parámetro amount no puede ser negativo o cero")
	@NotNull(message = "El parámetro amount no puede ir vacío")
	private BigDecimal amount;
	@Getter
	@Setter
	@NotNull(message = "El parámetro status no puede ir vacío")
	private PaymentStatusEnum status;

	@Override
	public String toString() {
		return new Gson().toJson(this);
	}
}