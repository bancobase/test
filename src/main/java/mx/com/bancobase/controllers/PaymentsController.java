package mx.com.bancobase.controllers;

import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import mx.com.bancobase.business.PaymentsBusiness;
import mx.com.bancobase.dtos.PatchPaymentDto;
import mx.com.bancobase.dtos.PaymentDto;
import mx.com.bancobase.dtos.PaymentStatusDto;
import mx.com.bancobase.exceptions.BancoBaseException;
import mx.com.bancobase.exceptions.NotFoundException;
import mx.com.bancobase.exceptions.ParamTypeException;
import mx.com.bancobase.utils.BancoBaseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@Slf4j
@RequestMapping("${controllers.endpoints.payments.request}")
public class PaymentsController {

    private final PaymentsBusiness paymentsBusiness;
    @Autowired
    private PaymentsController(PaymentsBusiness paymentsBusiness){
        this.paymentsBusiness = paymentsBusiness;
    }

    @GetMapping("${controllers.endpoints.payments.status}")
    public ResponseEntity<PaymentStatusDto> getPayment(@PathVariable String operationId)
            throws ParamTypeException, NotFoundException {
        log.info("operationId {}", operationId);
        return ResponseEntity.ok(paymentsBusiness.getPayment(BancoBaseUtils.getIntegerVaue(operationId)));
    }
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity postPayment(@Valid @RequestBody PaymentDto paymentDto)
            throws NotFoundException, BancoBaseException {
        log.info("post {}", paymentDto);
        return paymentsBusiness.postPayment(paymentDto);
    }
    @PatchMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity patchStatusPayment(@Valid @RequestBody PatchPaymentDto paymentDto)
            throws NotFoundException, BancoBaseException {
        log.info("post {}", paymentDto);
        return paymentsBusiness.patchPayment(paymentDto);
    }
//    @GetMapping("${controllers.endpoints.permitAll.medical.login.administrator}")
//    public ModelAndView medicallogin() throws XookException {
//        log.info("login medadmin");
//        ClientLoginConfigurationEntity clientLoginConfigurationEntity =
//                accessBusiness.getLoginConfigurationMedicalAdmin();
//        ModelAndView loginModelAndView = new ModelAndView("login");
//        loginModelAndView.addObject("headerName", clientLoginConfigurationEntity.getHeaderName());
//        loginModelAndView.addObject("imageUrl", clientLoginConfigurationEntity.getImageUrl());
//        return loginModelAndView;
//    }
//    @GetMapping("${controllers.endpoints.permitAll.medical.login.webMedical}")
//    public ModelAndView cusomerlogin() {
//        log.info("login medical");
//        ModelAndView loginModelAndView = new ModelAndView("login");
//        loginModelAndView.addObject("systemClient", "medical");
//        return loginModelAndView;
//    }
//
//    @GetMapping("${controllers.endpoints.permitAll.medical.logout}")
//    public ModelAndView logout(@RequestParam("redirect_uri") String redirectUri) throws Exception {
//        http.logout(httpSecurityLogoutConfigurer -> httpSecurityLogoutConfigurer
//                .deleteCookies("JSESSIONID")
//                .invalidateHttpSession(true)
//                .clearAuthentication(true)
//        );
//        return new ModelAndView("redirect:" + redirectUri);
//    }
}
