package mx.com.bancobase.business.impl;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import mx.com.bancobase.business.PaymentsBusiness;
import mx.com.bancobase.dtos.PatchPaymentDto;
import mx.com.bancobase.dtos.PaymentDto;
import mx.com.bancobase.dtos.PaymentStatusDto;
import mx.com.bancobase.entities.Accounts;
import mx.com.bancobase.entities.Operations;
import mx.com.bancobase.entities.StatusOperationsCat;
import mx.com.bancobase.exceptions.BancoBaseException;
import mx.com.bancobase.exceptions.NotFoundException;
import mx.com.bancobase.kafka.producers.messages.PaymentMessageProducer;
import mx.com.bancobase.repository.AccountsRepository;
import mx.com.bancobase.repository.OperationsRepository;
import mx.com.bancobase.repository.StatusOperationsCatRepository;
import mx.com.bancobase.utils.BancoBaseUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class PaymentsBusinessImpl implements PaymentsBusiness {
	private final OperationsRepository operationsRepository;
	private final AccountsRepository accountsRepository;
    private final StatusOperationsCatRepository statusOperationsCatRepository;
    private PaymentMessageProducer paymentMessageProducer;
    public PaymentsBusinessImpl(OperationsRepository operationsRepository,
                                AccountsRepository accountsRepository,
                                StatusOperationsCatRepository statusOperationsCatRepository
            ,
                                PaymentMessageProducer paymentMessageProducer) {
        this.operationsRepository = operationsRepository;
        this.accountsRepository = accountsRepository;
        this.statusOperationsCatRepository = statusOperationsCatRepository;
        this.paymentMessageProducer = paymentMessageProducer;
    }

    @Override
    public PaymentStatusDto getPayment(Integer operationId) throws NotFoundException {
        Operations operation = operationsRepository.findById(operationId).orElseThrow(
                ()->new NotFoundException("No se encontró información"));

        return PaymentStatusDto.builder()
                .status(operation.getStatus().getName())
                .operationDate(operation.getOperationDate())
                .concept(operation.getConcept())
                .originAccount(operation.getTransmitter().getAccountId())
                .originNameAccount(operation.getTransmitter().getUser().getName())
                .targetAccount(operation.getReceptor().getAccountId())
                .targetNameAccount(operation.getReceptor().getUser().getName())
                .amount(operation.getAmount())
                .build();
    }

    @Override
    public ResponseEntity<Integer> postPayment(PaymentDto paymentDto) throws NotFoundException, BancoBaseException {

        if(paymentDto.getOriginAccount().equals(paymentDto.getTargetAccount())){
            throw new BancoBaseException("La cuenta emisora y receptora deben ser diferentes");
        }
        Accounts originAccount = accountsRepository.findById(paymentDto.getOriginAccount())
                .orElseThrow(()->new NotFoundException("No se encontró la cuenta emisora"));
        Accounts targetAccount = accountsRepository.findById(paymentDto.getTargetAccount())
                .orElseThrow(()->new NotFoundException("No se encontró la cuenta receptora"));
        StatusOperationsCat status = statusOperationsCatRepository.findByKeyName(paymentDto.getStatus().name())
                .orElseThrow(()->new NotFoundException("No se encontró un estatus válido"));
        Operations operation = Operations.builder()
                .operationDate(BancoBaseUtils.getCurrentDate())
                .amount(paymentDto.getAmount())
                .receptor(targetAccount)
                .transmitter(originAccount)
                .concept(paymentDto.getConcept())
                .status(status).build();
        operation = operationsRepository.save(operation);
        return ResponseEntity.ok(operation.getAccountId());
    }

    @Override
    public ResponseEntity<HttpStatus> patchPayment(PatchPaymentDto paymentDto)
            throws NotFoundException {
        StatusOperationsCat status = statusOperationsCatRepository.findByKeyName(paymentDto.getStatus().name())
                .orElseThrow(()->new NotFoundException("No se encontró un estatus válido"));
        Operations operation = operationsRepository.findById(paymentDto.getAccountId())
                .orElseThrow(()->new NotFoundException("No se encontró la transacción"));
        operation.setStatus(status);
        operationsRepository.save(operation);
        paymentMessageProducer.sendMessage("status", new Gson().toJson(operation));
        return ResponseEntity.ok(HttpStatus.ACCEPTED);
    }
}
