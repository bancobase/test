package mx.com.bancobase.business;

import mx.com.bancobase.dtos.PatchPaymentDto;
import mx.com.bancobase.dtos.PaymentDto;
import mx.com.bancobase.dtos.PaymentStatusDto;
import mx.com.bancobase.exceptions.BancoBaseException;
import mx.com.bancobase.exceptions.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public interface PaymentsBusiness {
    PaymentStatusDto getPayment(Integer operationId) throws NotFoundException;

    ResponseEntity<Integer> postPayment(PaymentDto paymentDto) throws NotFoundException, BancoBaseException;
    ResponseEntity<HttpStatus> patchPayment(PatchPaymentDto paymentDto) throws NotFoundException, BancoBaseException;
}