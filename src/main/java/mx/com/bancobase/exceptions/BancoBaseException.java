package mx.com.bancobase.exceptions;

import java.io.Serial;

public class BancoBaseException extends Exception{

    @Serial
    private static final long serialVersionUID = 1L;
    public BancoBaseException(String msg) {
        super(msg);
    }
}
