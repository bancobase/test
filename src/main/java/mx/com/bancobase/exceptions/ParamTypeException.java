package mx.com.bancobase.exceptions;

import java.io.Serial;

public class ParamTypeException extends Exception{

    @Serial
    private static final long serialVersionUID = 1L;
    public ParamTypeException(String msg) {
        super(msg);
    }
}
