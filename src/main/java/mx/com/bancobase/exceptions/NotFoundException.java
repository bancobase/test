package mx.com.bancobase.exceptions;

import java.io.Serial;

public class NotFoundException extends Exception{

    @Serial
    private static final long serialVersionUID = 1L;
//    public NotFoundException() {
//        super("Error: ");
//    }
    public NotFoundException(String msg) {
        super(msg);
    }
}
