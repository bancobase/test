package mx.com.bancobase.exceptions;

import jakarta.el.PropertyNotFoundException;
import lombok.extern.slf4j.Slf4j;
import mx.com.bancobase.dtos.GeneralMsgDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@Slf4j
public class ControllerExceptionHandler {

    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public GeneralMsgDto globalExceptionHandler(Exception ex) {
        log.error("Exception native {}", ex.getMessage());
        ex.printStackTrace();
        return resultMessage(HttpStatus.INTERNAL_SERVER_ERROR, ex);
    }
    @ExceptionHandler(RuntimeException.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public GeneralMsgDto globalRuntimeExceptionHandler(RuntimeException ex) {
        log.error("RuntimeException native {}", ex.getMessage());
        ex.printStackTrace();
        return resultMessage(HttpStatus.INTERNAL_SERVER_ERROR, ex);
    }
    @ExceptionHandler(BancoBaseException.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public GeneralMsgDto xookException(BancoBaseException ex) {
        log.error("BancoBaseException {}", ex.getMessage());
        return resultMessage(HttpStatus.INTERNAL_SERVER_ERROR, ex);
    }
    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public GeneralMsgDto xookException(NotFoundException ex) {
        log.error("BancoBaseException {}", ex.getMessage());
        return resultMessage(HttpStatus.NO_CONTENT, ex);
    }
    @ExceptionHandler(ParamTypeException.class)
    @ResponseStatus(value = HttpStatus.CONFLICT)
    public GeneralMsgDto xookException(ParamTypeException ex) {
        log.error("ParamTypeException {}", ex.getMessage());
        return resultMessage(HttpStatus.CONFLICT, ex);
    }
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public GeneralMsgDto propertyNotFound(MethodArgumentNotValidException ex) {
        log.error("MethodArgumentNotValidException {}", ex.getMessage());
        return resultMessage(HttpStatus.NO_CONTENT, ex);
    }
    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public GeneralMsgDto enumTypeException(HttpMessageNotReadableException ex) {
        log.error("HttpMessageNotReadableException {}", ex.getMessage());
        return resultMessage(HttpStatus.NO_CONTENT, ex);
    }

    private GeneralMsgDto resultMessage ( HttpStatus status, Exception exception){

        if( exception instanceof BancoBaseException ||
                exception instanceof NotFoundException ||
                exception instanceof PropertyNotFoundException ||
                exception instanceof ParamTypeException
        ){
            return GeneralMsgDto.builder()
                    .msg(exception.getMessage())
                    .httpStatus(status)
                    .build();
        }else if( exception instanceof MethodArgumentNotValidException){
            return GeneralMsgDto.builder()
                    .msg(((MethodArgumentNotValidException)exception).getFieldError().getDefaultMessage())
                    .httpStatus(status)
                    .build();

        }else if( exception instanceof HttpMessageNotReadableException &&
                exception.getMessage().contains("mx.com.bancobase.enums.PaymentStatusEnum")){
            return GeneralMsgDto.builder()
                    .msg("Parámetro status inválido PAID, CANCELLED or REFUSED")
                    .httpStatus(status)
                    .build();

        }
        return GeneralMsgDto.builder()
                .msg("Error")
                .httpStatus(status)
                .build();

    }
}