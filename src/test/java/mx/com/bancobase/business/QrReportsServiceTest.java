package mx.com.bancobase.business;

import com.fasterxml.jackson.core.JsonProcessingException;
import mx.com.bancobase.business.impl.PaymentsBusinessImpl;
import mx.com.bancobase.dtos.PatchPaymentDto;
import mx.com.bancobase.dtos.PaymentDto;
import mx.com.bancobase.entities.Accounts;
import mx.com.bancobase.entities.Operations;
import mx.com.bancobase.entities.StatusOperationsCat;
import mx.com.bancobase.entities.Users;
import mx.com.bancobase.enums.PaymentStatusEnum;
import mx.com.bancobase.kafka.producers.messages.PaymentMessageProducer;
import mx.com.bancobase.repository.AccountsRepository;
import mx.com.bancobase.repository.OperationsRepository;
import mx.com.bancobase.repository.StatusOperationsCatRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.when;

@SpringBootTest
class QrReportsServiceTest {

//    @Autowired
    PaymentsBusiness paymentsBusiness;
    OperationsRepository operationsRepository;
    AccountsRepository accountsRepository;
    StatusOperationsCatRepository statusOperationsCatRepository;
    private PaymentMessageProducer paymentMessageProducer;
    Operations operation;
    Accounts accounts;
    StatusOperationsCat statusOperationsCat;
    @BeforeEach
    public void setup(){
        operationsRepository = Mockito.mock(OperationsRepository.class);
        accountsRepository = Mockito.mock(AccountsRepository.class);
        statusOperationsCatRepository = Mockito.mock(StatusOperationsCatRepository.class);
        paymentMessageProducer = Mockito.mock(PaymentMessageProducer.class);
        paymentsBusiness = new PaymentsBusinessImpl(operationsRepository, accountsRepository,
                statusOperationsCatRepository, paymentMessageProducer);
    }
    public QrReportsServiceTest() throws JsonProcessingException {
        accounts = new Accounts();
        accounts.setAccountId(1);
        accounts.setCreationAccount(new Date(12345678));
        Users user = new Users();
        user.setUserId(1);
        user.setName("user");
        accounts.setUser(user);
        accounts.setAmount(new BigDecimal(100));
        statusOperationsCat = new StatusOperationsCat();
        statusOperationsCat.setStatusOpId((short) 1);
        statusOperationsCat.setName("Status");
        statusOperationsCat.setKeyName("STATUS");
        operation = Operations.builder()
                .operationDate(new Date(12345678))
                .amount(new BigDecimal(100))
                .receptor(accounts)
                .transmitter(accounts)
                .accountId(1)
                .status(statusOperationsCat)
                .build();
        statusOperationsCat = new StatusOperationsCat();
        statusOperationsCat.setStatusOpId((short)1);
        statusOperationsCat.setName("NAME");
        statusOperationsCat.setKeyName("KEYNAME");
    }
    @Test
    void getPayment() throws Exception {
        Answer<Optional<Operations>> answer = invocation -> Optional.of(operation);

        when(operationsRepository.findById(anyInt()))
                .then(answer);

        assertEquals("{\"originAccount\":1,\"originNameAccount\":\"user\",\"targetAccount\":1," +
                        "\"targetNameAccount\":\"user\",\"amount\":100,\"status\":\"Status\"," +
                        "\"operationDate\":\"dic. 31, 1969\"}",
                paymentsBusiness.getPayment(1).toString());
    }
    @Test
    void postPayment() throws Exception {
        Answer<Optional<Accounts>> accountsAnswer = invocation -> Optional.of(this.accounts);
        Answer<Optional<StatusOperationsCat>> statusCat = invocation -> Optional.of(statusOperationsCat);
        Answer<Operations> operationAnswer = invocation -> this.operation;

        when(accountsRepository.findById(anyInt())).then(accountsAnswer);
        when(statusOperationsCatRepository.findByKeyName(anyString())).then(statusCat);
        when(operationsRepository.save(any())).then(operationAnswer);


        PaymentDto paymentDto = new PaymentDto().builder()
                .concept("test")
                .originAccount(100)
                .targetAccount(200)
                .status(PaymentStatusEnum.TRANSFERRED)
                .amount(new BigDecimal(150))
                .build();

        assertEquals("<200 OK OK,1,[]>",
                paymentsBusiness.postPayment(paymentDto).toString());
    }
    @Test
    void patchPayment() throws Exception {
        Answer<Optional<StatusOperationsCat>> statusCat = invocation -> Optional.of(statusOperationsCat);
        Answer<Optional<Operations>> operationAnswer = invocation -> Optional.of(this.operation);

        when(statusOperationsCatRepository.findByKeyName(anyString())).then(statusCat);
        when(operationsRepository.findById(anyInt())).then(operationAnswer);


        PatchPaymentDto paymentDto = new PatchPaymentDto().builder()
                .accountId(1)
                .status(PaymentStatusEnum.TRANSFERRED)
                .build();

        assertEquals("<200 OK OK,202 ACCEPTED,[]>",
                paymentsBusiness.patchPayment(paymentDto).toString());
    }
}
