## Previa configuración

para levantar la la base de datos MySql dirigirse al path del proyecto en la carpeta "db". Eejecutar docker-compose up -d

Ejemplo:

PATH_PROYECTO/bd % docker-compose up -d

## KafKa

para levantar KAFKA ir al path del proyecto de la carpeta "kafka" y ejecutar docker-compose up -d

Ejemplo:

PATH_PROYECTO/bd % docker-compose up -d

## Generar docker image

Ejecutar las siguientes opciones en consola

PATH_PROYECTO % gradle build

PATH_PROYECTO % docker build -t bancobase-docker .

PATH_PROYECTO % docker run bancobase-docker


## Nota
Las cuentas para realizar una nueva alta deben ser 25654 y 25655