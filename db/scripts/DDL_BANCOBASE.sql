--
-- MYSQL BANCOBASE DATABASE
CREATE TABLE sex_cat (sex_cat_id SMALLINT NOT NULL AUTO_INCREMENT,
                    name VARCHAR(20),
                    PRIMARY KEY(sex_cat_id));

CREATE TABLE status_operation_cat (status_op_id SMALLINT NOT NULL AUTO_INCREMENT,
                      name VARCHAR(20),
                      key_name VARCHAR(20),
                      PRIMARY KEY(status_op_id));

CREATE TABLE users (user_id INT NOT NULL AUTO_INCREMENT,
                    name VARCHAR(20),
                    lastname VARCHAR(20),
                    sex SMALLINT,
                    birth DATE,
                    PRIMARY KEY(user_id),
                    FOREIGN KEY(sex) REFERENCES sex_cat(sex_cat_id));
CREATE TABLE accounts (account_id INT NOT NULL AUTO_INCREMENT,
                        user_id INT,
                        amount DECIMAL(60,2),
                        creation_account DATE,
                        PRIMARY KEY(account_id),
                        FOREIGN KEY(user_id) REFERENCES users(user_id)
)AUTO_INCREMENT=25654;
CREATE TABLE operations (operation_id INT NOT NULL AUTO_INCREMENT,
                         account_id_transmitter INT,
                         account_id_receptor INT,
                         amount DECIMAL(60,2),
                         concept VARCHAR(60),
                         status SMALLINT,
                         operation_date DATE,
                         PRIMARY KEY(operation_id),
    FOREIGN KEY(account_id_transmitter) REFERENCES accounts(account_id),
    FOREIGN KEY(account_id_receptor) REFERENCES accounts(account_id),
    FOREIGN KEY(status) REFERENCES status_operation_cat(status_op_id)
)AUTO_INCREMENT=1000;