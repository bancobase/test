
INSERT INTO sex_cat (name) VALUES ('Hombre');
INSERT INTO sex_cat (name) VALUES ('Mujer');

INSERT INTO status_operation_cat (name, key_name) VALUES('Aprobado','APPROVED');
INSERT INTO status_operation_cat (name, key_name) VALUES('Cancelado','CANCELLED');
INSERT INTO status_operation_cat (name, key_name) VALUES('Devuelto','REFUNDED');
INSERT INTO status_operation_cat (name, key_name) VALUES('Fondos insuficientes', 'INSUFFICIENT_FUNDS');

INSERT INTO users (name, lastname, sex, birth) VALUES ('Hector', 'Copca',
                               (SELECT sex_cat_id FROM sex_cat WHERE name = 'Hombre'), '1984-03-29');
INSERT INTO users (name, lastname, sex, birth) VALUES ('Minerva', 'Castro',
                               (SELECT sex_cat_id FROM sex_cat WHERE name = 'Mujer'), '1990-06-18');

INSERT INTO accounts (user_id, amount, creation_account) VALUES(
                            (SELECT user_id FROM users WHERE name = 'Hector' and lastname = 'Copca'),
                             2250.70, '2023-01-20');
INSERT INTO accounts (user_id, amount, creation_account) VALUES(
                            (SELECT user_id FROM users WHERE name = 'Minerva' and lastname = 'Castro'),
                            2250.70, '2023-01-20');